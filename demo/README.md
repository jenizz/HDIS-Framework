<p align="center"><a align="center" href='https://gitee.com/w6513017/HDIS-Framework'><img src='https://gitee.com/w6513017/HDIS-Framework/widgets/widget_3.svg' alt='Fork me on Gitee'></img></a></p>
<h1 align="center">HDIS-Demo</h1>
<p align="center">针对<a href="https://gitee.com/w6513017/HDIS-Framework"><strong>《HDIS-Framework》</strong></a>的Demo</p>

# HDIS-Demo
## 项目介绍
针对[HDIS-Framework](https://gitee.com/w6513017/HDIS-Framework)的Demo<br>
## 项目结构
### 目录src/main/java
tech.hdis.demo.controller：关于MVC方面的，swagger使用、返回值规约、输入异常验证、异常转化、异常处理、数据字典使用、日志使用等使用例子。<br>
tech.hdis.demo.jpa：关于JPA方面的、增删查改、分页组件使用例子。<br>
tech.hdis.demo.mongo：关于MongoDB方面的、增删查改、分页组件使用例子。<br>
tech.hdis.demo.push：云端推送相关例子，默认实现采用阿里云推。<br>
### 目录src/main/resources
application-jpa.properties:JPA的MySql相关配置。<br>
application-mongo.properties:MongoDB相关配置<br>
application-mvc.properties:MVC相关的，swagger、错误码、数据字典相关配置。<br>
application-push.properties:推送相关的，阿里云推的相关配置。<br>
### 目录src/test/java
tech.hdis.test.demo.controller：针对tech.hdis.demo.controller的单元测试。<br>
tech.hdis.test.demo.jpa：针对tech.hdis.demo.jpa的单元测试。<br>
tech.hdis.test.demo.mongo：针对tech.hdis.demo.mongo的单元测试。<br>
tech.hdis.test.demo.push：针对tech.hdis.demo.push的单元测试。<br>