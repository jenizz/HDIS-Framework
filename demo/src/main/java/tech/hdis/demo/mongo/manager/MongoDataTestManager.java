package tech.hdis.demo.mongo.manager;

import org.springframework.stereotype.Component;
import tech.hdis.demo.mongo.entity.MongoDataTest;
import tech.hdis.demo.mongo.repository.MongoDataTestRepository;
import tech.hdis.framework.data.mongo.manager.MongoDataManager;

/**
 * Manager用例
 *
 * @author 黄志文
 */
@Component
public class MongoDataTestManager extends MongoDataManager<MongoDataTestRepository, MongoDataTest> {
}