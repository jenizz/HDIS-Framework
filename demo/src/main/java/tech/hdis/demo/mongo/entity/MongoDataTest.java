package tech.hdis.demo.mongo.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import tech.hdis.framework.data.mongo.po.MongoDataEntity;

/**
 * 实体用例
 *
 * @author 黄志文
 */
@Getter
@Setter
@Document(collection = "data_test")
public class MongoDataTest extends MongoDataEntity {
    @Field("name")
    private String name;
}
