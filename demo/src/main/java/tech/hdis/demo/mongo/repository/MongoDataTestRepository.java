package tech.hdis.demo.mongo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tech.hdis.demo.mongo.entity.MongoDataTest;
import tech.hdis.framework.data.mongo.repository.MongoPoRepository;

import java.util.List;

/**
 * repository用例
 *
 * @author 黄志文
 */
public interface MongoDataTestRepository extends MongoPoRepository<MongoDataTest> {
    /**
     * 根据名称查询
     *
     * @param name     名称
     * @param pageable 分页
     * @return 查询结果
     */
    Page<MongoDataTest> findByName(String name, Pageable pageable);

    /**
     * 根据名称查询
     *
     * @param name 名称
     * @return 查询结果
     */
    List<MongoDataTest> findByName(String name);
}
