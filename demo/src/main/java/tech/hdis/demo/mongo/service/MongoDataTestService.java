package tech.hdis.demo.mongo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import tech.hdis.demo.mongo.entity.MongoDataTest;
import tech.hdis.demo.mongo.manager.MongoDataTestManager;
import tech.hdis.framework.page.PageBuilder;
import tech.hdis.framework.page.PageInfo;

import java.util.List;

/**
 * Manager用例
 *
 * @author 黄志文
 */
@Component
public class MongoDataTestService {
    private static final String NAME = "黄志文";
    @Autowired
    private MongoDataTestManager mongoDataTestManager;

    public void save() {
        MongoDataTest mongoDataTest = new MongoDataTest();
        mongoDataTest.setName(NAME);
        mongoDataTestManager.save(mongoDataTest);
    }

    public void delete() {
        List<MongoDataTest> mongoDataTests = mongoDataTestManager.repository.findByName(NAME);
        mongoDataTestManager.repository.delete(mongoDataTests);
    }

    public PageInfo findByName() {
        PageRequest pageRequest = PageBuilder.pageRequest(1);
        Page page = mongoDataTestManager.repository.findByName(NAME, pageRequest);
        PageInfo pageInfo = PageBuilder.pageInfo(page);
        System.out.println(pageInfo);
        return pageInfo;
    }
}