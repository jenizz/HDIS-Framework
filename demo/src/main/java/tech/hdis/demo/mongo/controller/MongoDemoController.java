package tech.hdis.demo.mongo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.hdis.framework.response.RestfulResponse;

/**
 * MongoDemo例子
 *
 * @author 黄志文
 */
@Api("demo例子")
@RestController
@RequestMapping("/demo/mongo")
public class MongoDemoController {

    @ApiOperation(value = "成功返回")
    @GetMapping("/success")
    public RestfulResponse success() {
        return RestfulResponse.getInstance().success();
    }
}
