package tech.hdis.demo.cache.repository;

import com.alicp.jetcache.anno.Cached;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import tech.hdis.demo.cache.entity.CacheDataTest;

/**
 * repository用例
 *
 * @author 黄志文
 */
@Slf4j
@Repository
public class CacheTestRepository {
    @Cached(name = "CacheTestService.findByName", expire = 3600)
    public CacheDataTest findByName() {
        log.info("执行CacheTestRepository！");
        return new CacheDataTest();
    }
}
