package tech.hdis.demo.cache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.hdis.demo.cache.service.CacheTestService;
import tech.hdis.framework.response.RestfulResponse;

/**
 * demo例子
 *
 * @author 黄志文
 */
@Api("demo例子")
@Slf4j
@RestController
@RequestMapping("/demo/cache")
public class CacheDemoController {

    //使用说明文档请查看：https://github.com/alibaba/jetcache/wiki/Home_CN

    @Autowired
    private CacheTestService cacheTestService;

    @ApiOperation(value = "缓存")
    @GetMapping("/test")
    public RestfulResponse success() {
        log.info("执行/demo/cache/test");
        cacheTestService.findByName();
        return RestfulResponse.getInstance().success();
    }

}
