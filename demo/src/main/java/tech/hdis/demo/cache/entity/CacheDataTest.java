package tech.hdis.demo.cache.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CacheDataTest {
    private String name = "黄志文";
}
