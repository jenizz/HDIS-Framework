package tech.hdis.demo.cache.service;

import com.alicp.jetcache.anno.Cached;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.hdis.demo.cache.entity.CacheDataTest;
import tech.hdis.demo.cache.repository.CacheTestRepository;

/**
 * repository用例
 *
 * @author 黄志文
 */
@Slf4j
@Service
public class CacheTestService {

    @Autowired
    private CacheTestRepository cacheTestRepository;

    public CacheDataTest findByName() {
        log.info("执行CacheTestService！");
        return cacheTestRepository.findByName();
    }
}
