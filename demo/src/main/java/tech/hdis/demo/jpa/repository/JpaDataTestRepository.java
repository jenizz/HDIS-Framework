package tech.hdis.demo.jpa.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import tech.hdis.demo.jpa.entity.JpaDataTest;
import tech.hdis.framework.data.jpa.repository.JpaPoRepository;

import java.util.List;


/**
 * repository用例
 *
 * @author 黄志文
 */
public interface JpaDataTestRepository extends JpaPoRepository<JpaDataTest> {
    /**
     * 根据名称查询
     *
     * @param name     名称
     * @param pageable 分页
     * @return 查询结果
     */
    Page<JpaDataTest> findByName(String name, Pageable pageable);

    /**
     * 根据名称查询
     *
     * @param name 名称
     * @return 查询结果
     */
    List<JpaDataTest> findByName(String name);
}
