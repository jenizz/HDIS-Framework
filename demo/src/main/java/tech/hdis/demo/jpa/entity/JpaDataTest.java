package tech.hdis.demo.jpa.entity;


import lombok.Getter;
import lombok.Setter;
import tech.hdis.framework.data.jpa.po.JpaDataEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 实体用例
 *
 * @author 黄志文
 */
@Getter
@Setter
@Entity
@Table(name = "t_data_test")
public class JpaDataTest extends JpaDataEntity {
    @Column
    private String name;
}
