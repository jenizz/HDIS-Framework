package tech.hdis.demo.jpa.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tech.hdis.demo.jpa.service.JpaDataTestService;
import tech.hdis.framework.page.PageInfo;
import tech.hdis.framework.response.RestfulResponse;

/**
 * JpaDemo例子
 *
 * @author 黄志文
 */
@Api("demo例子")
@RestController
@RequestMapping("/demo/jpa")
public class JpaDemoController {

    @Autowired
    private JpaDataTestService jpaDataTestService;

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public RestfulResponse success() {
        jpaDataTestService.save();
        return RestfulResponse.getInstance().success();
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/delete")
    public RestfulResponse delete() {
        jpaDataTestService.delete();
        return RestfulResponse.getInstance().success();
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("/findByName")
    public RestfulResponse findByName() {
        PageInfo pageInfo = jpaDataTestService.findByName();
        return RestfulResponse.getInstance().success().result(pageInfo);
    }
}
