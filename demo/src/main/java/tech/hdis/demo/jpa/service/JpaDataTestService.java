package tech.hdis.demo.jpa.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import tech.hdis.demo.jpa.entity.JpaDataTest;
import tech.hdis.demo.jpa.repository.JpaDataTestRepository;
import tech.hdis.framework.page.PageBuilder;
import tech.hdis.framework.page.PageInfo;

import java.util.List;

/**
 * service用例
 *
 * @author 黄志文
 */
@Service
public class JpaDataTestService {
    private static final String NAME = "黄志文";
    @Autowired
    private JpaDataTestRepository jpaDataTestRepository;

    public void save() {
        JpaDataTest jpaDataTest = new JpaDataTest();
        jpaDataTest.setName(NAME);
        jpaDataTestRepository.save(jpaDataTest);
    }

    public void delete() {
        List<JpaDataTest> jpaDataTests = jpaDataTestRepository.findByName(NAME);
        jpaDataTestRepository.delete(jpaDataTests);
    }

    public PageInfo findByName() {
        PageRequest pageRequest = PageBuilder.pageRequest(1);
        Page page = jpaDataTestRepository.findByName(NAME, pageRequest);
        PageInfo pageInfo = PageBuilder.pageInfo(page);
        System.out.println(pageInfo);
        return pageInfo;
    }
}
