package tech.hdis.demo.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.hdis.framework.exception.exceptions.BusinessException;
import tech.hdis.framework.sms.AuthCode;

/**
 * 短信验证码服务
 *
 * @author 黄志文
 */
@Service
public class SmsService {

    /**
     * 短信验证码接口
     */
    @Autowired
    private AuthCode authCode;

    /**
     * 发送短信验证码
     * @return 是否成功
     */
    public Boolean sendSms() {
        Boolean result = false;
        try {
            authCode.sendAuthCode("你的电话号码");
        } catch (Exception e) {
            throw new BusinessException("发送验证码出现异常！", e);
        }
        return result;
    }

    /**
     * 验证短信验证码
     * @return 是否通过验证
     */
    public Boolean validationAuthCode() {
        return authCode.validationAuthCode("你的电话号码", "你的验证码");
    }
}
