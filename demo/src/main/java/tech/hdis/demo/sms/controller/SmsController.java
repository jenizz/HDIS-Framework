package tech.hdis.demo.sms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.hdis.demo.sms.service.SmsService;
import tech.hdis.framework.response.RestfulResponse;

/**
 * PushDemo例子
 *
 * @author 黄志文
 */
@Api("demo例子")
@RestController
@RequestMapping("/demo/sms")
public class SmsController {

    @Autowired
    private SmsService smsService;

    @ApiOperation(value = "推送例子")
    @GetMapping("/sendSms")
    public RestfulResponse sendSms() {
        Boolean result = smsService.sendSms();
        return RestfulResponse.getInstance().success().result(result);
    }
}
