package tech.hdis.demo;

import com.alicp.jetcache.anno.config.EnableMethodCache;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;

/**
 * 启动类<br>
 * 需要特别添加扫描‘tech.hdis.framework’包<br>
 * 添加需要扫描的JPA的Repository包<br>
 * 添加需要扫描的Mongo的Repository包<br>
 *
 * @author 黄志文
 */
@SpringBootApplication
@ComponentScan({"tech.hdis.framework", "tech.hdis.demo"})
@EnableMethodCache(basePackages = "tech.hdis.demo")
@EnableJpaRepositories(basePackages = "tech.hdis.demo.jpa.repository")
@EnableMongoRepositories(basePackages = "tech.hdis.demo.mongo.repository")
@PropertySource(value = {
        "classpath:application-cache.properties",
        "classpath:application-mvc.properties",
        "classpath:application-jpa.properties",
        "classpath:application-mongo.properties",
        "classpath:application-push.properties",
        "classpath:application-sms.properties",
})
public class Application {

    private static final String TOKEN = "ea10f2a00e3e4d96a7a220b0a390a7c2";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        log.info("================================================================================");
        log.info("Application Started Finish , swagger url : {}", "http://localhost:8080/swagger-ui.html");
        log.info("================================================================================");
    }

    /**
     * 自定义swagger全局参数
     *
     * @return ParameterBuilder
     */
    @Bean
    public ParameterBuilder parameterBuilder() {
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        parameterBuilder.name("token")
                .description("权限令牌")
                .defaultValue(TOKEN)
                .modelRef(new ModelRef("string"))
                .parameterType("query")
                .required(true);
        return parameterBuilder;
    }
}
