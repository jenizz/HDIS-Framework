package tech.hdis.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.hdis.framework.dictionary.Dic;
import tech.hdis.framework.exception.exceptions.BusinessException;
import tech.hdis.framework.response.RestfulResponse;

import java.util.Arrays;
import java.util.List;

/**
 * demo例子
 *
 * @author 黄志文
 */
@Slf4j
@Api("demo例子")
@RestController
@RequestMapping("/demo")
public class DemoController {

    @ApiOperation(value = "成功返回")
    @GetMapping("/success")
    public RestfulResponse success() {
        log.info("我是日志");
        return RestfulResponse.getInstance().success();
    }

    @ApiOperation(value = "失败返回")
    @GetMapping("/fail")
    public RestfulResponse fail() {
        //C401是配置文件指定的返回码。
        //所有错误返回不可写返回值，只能指定返回码。
        return RestfulResponse.getInstance().code("C401");
    }

    @ApiOperation(value = "返回单种数据")
    @GetMapping("/resultSingle")
    public RestfulResponse resultSingle() {
        String result = "我是单种数据，支持任何类型";
        return RestfulResponse.getInstance().success().result(result);
    }

    @ApiOperation(value = "返回多种数据")
    @GetMapping("/resultMultiple")
    public RestfulResponse resultMultiple() {
        String result = "我是多种数据，支持任何类型，但需要自定义key";
        List<String> strings = Arrays.asList(result, result, result);
        return RestfulResponse.getInstance().success().result("one", result).result("two", strings);
    }

    @ApiOperation(value = "返回数据字典数据")
    @GetMapping("/dic")
    public RestfulResponse dic() {
        String dic = Dic.get("dic");
        return RestfulResponse.getInstance().success().result(dic);
    }

    @ApiOperation(value = "默认异常处理")
    @GetMapping("/defaultException")
    public RestfulResponse defaultException() {
        throw new NullPointerException();
    }

    @ApiOperation(value = "业务异常处理")
    @GetMapping("/businessException")
    public RestfulResponse businessException() {
        throw new BusinessException("我是主动抛出的业务异常");
    }

    @ApiOperation(value = "验证异常处理")
    @PostMapping("/validatorException")
    public RestfulResponse validatorException(@Validated DemoDTO demoDTO) {
        return RestfulResponse.getInstance().success();
    }
}
