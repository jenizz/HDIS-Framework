package tech.hdis.demo.controller;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

/**
 * DemoDTO
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
public class DemoDTO {
    @NotBlank(message = "名字不能为空！")
    private String name;
}
