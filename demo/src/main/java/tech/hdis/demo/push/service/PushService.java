package tech.hdis.demo.push.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.hdis.framework.exception.exceptions.BusinessException;
import tech.hdis.framework.push.Push;
import tech.hdis.framework.push.PushBody;

/**
 * 推送服务
 *
 * @author 黄志文
 */
@Service
public class PushService {

    /**
     * 推送接口
     */
    @Autowired
    private Push push;

    /**
     * 单个推送
     *
     * @return 是否成功
     */
    public Boolean push() {
        try {
            return push.push(PushBody.getInstance().msg("推送的消息").toJson(), "推送目标别名（ALIAS）", "推送目标别名（ALIAS）");
        } catch (Exception e) {
            throw new BusinessException("推送出现异常！", e);
        }
    }

    /**
     * 全体推送
     *
     * @return 是否成功
     */
    public Boolean pushAll() {
        try {
            return push.pushAll(PushBody.getInstance().msg("推送的消息").toJson());
        } catch (Exception e) {
            throw new BusinessException("推送出现异常！", e);
        }
    }
}