package tech.hdis.demo.push.controller;

import com.aliyuncs.push.model.v20160801.PushResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.hdis.demo.push.service.PushService;
import tech.hdis.framework.response.RestfulResponse;

/**
 * PushDemo例子
 *
 * @author 黄志文
 */
@Api("demo例子")
@RestController
@RequestMapping("/demo/push")
public class PushController {

    @Autowired
    private PushService pushService;

    @ApiOperation(value = "推送例子")
    @GetMapping("/doPush")
    public RestfulResponse doPush() {
        PushResponse pushResponse = pushService.doPush();
        return RestfulResponse.getInstance().success().result(pushResponse);
    }
}
