package tech.hdis.demo.push.service;

import com.aliyuncs.exceptions.ClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.hdis.framework.exception.exceptions.BusinessException;
import tech.hdis.framework.push.aliyun.AliyunPushImpl;
import tech.hdis.framework.push.aliyun.CustomPushRequest;

/**
 * 阿里云推送服务
 *
 * @author 黄志文
 */
@Service
public class AliyunPushService {

    /**
     * 推送接口
     */
    @Autowired
    private AliyunPushImpl aliyunPush;

    /**
     * 得到推送认证信息<br>
     * 用于传递给客户端进行推送信息认证<br>
     *
     * @return 推送认证信息
     */
    public Object getPushAuthenticationInfo() {
        return aliyunPush.getPushAuthenticationInfo();
    }

    /**
     * 自定义推送<br>
     * 详情请查看：https://help.aliyun.com/knowledge_detail/48089.html<br>
     *
     * @return 是否成功
     */
    public Boolean push() {
        //CustomPushRequest继承了阿里云的PushRequest，可以定制化推送的内容。
        CustomPushRequest customPushRequest = CustomPushRequest.getInstance();
        try {
            return aliyunPush.push(customPushRequest);
        } catch (ClientException e) {
            throw new BusinessException("推送出现异常！", e);
        }
    }
}