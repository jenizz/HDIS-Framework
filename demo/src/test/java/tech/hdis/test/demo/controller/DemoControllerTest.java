package tech.hdis.test.demo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tech.hdis.demo.Application;
import tech.hdis.framework.test.JsonPath;
import tech.hdis.framework.test.MockRestfulRequestFactory;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class DemoControllerTest {

    private static final String PARENT_PATH = "/demo";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void successTest() throws Exception {
        mockMvc.perform(MockRestfulRequestFactory.get(PARENT_PATH + "/success"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.CODE).exists())
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.MESSAGE).exists());
    }
}
