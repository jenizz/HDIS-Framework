package tech.hdis.test.demo.jpa;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tech.hdis.demo.Application;
import tech.hdis.framework.test.JsonPath;
import tech.hdis.framework.test.MockRestfulRequestFactory;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class DemoJpaTest {

    private static final String PARENT_PATH = "/demo/jpa";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void saveTest() throws Exception {
        mockMvc.perform(MockRestfulRequestFactory.post(PARENT_PATH + "/save", ""))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.CODE).value("hdis.response.success.success"))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.MESSAGE).value("成功"));
    }

    @After
    public void deleteTest() throws Exception {
        mockMvc.perform(MockRestfulRequestFactory.delete(PARENT_PATH + "/delete"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.CODE).value("hdis.response.success.success"))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.MESSAGE).value("成功"));
    }

    @Test
    public void findByNameTest() throws Exception {
        mockMvc.perform(MockRestfulRequestFactory.get(PARENT_PATH + "/findByName"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.CODE).value("hdis.response.success.success"))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.MESSAGE).value("成功"))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.PAGE_INFO + "createTime").exists());
    }
}
