package tech.hdis.test.demo.mongo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tech.hdis.demo.Application;
import tech.hdis.demo.mongo.service.MongoDataTestService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class DemoMongoTest {

    @Autowired
    private MongoDataTestService mongoDataTestService;

    @Before
    public void saveTest() {
        mongoDataTestService.save();
    }

    @Test
    public void findByNameTest() {
        mongoDataTestService.findByName();
    }

    @After
    public void deleteTest() {
        mongoDataTestService.delete();
    }
}
