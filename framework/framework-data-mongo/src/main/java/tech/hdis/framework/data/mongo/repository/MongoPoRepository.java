package tech.hdis.framework.data.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;
import tech.hdis.framework.data.mongo.po.MongoDataEntity;

/**
 * MongoDB基础Repository
 *
 * @author 黄志文
 */
@NoRepositoryBean
public interface MongoPoRepository<T extends MongoDataEntity> extends MongoRepository<T, String> {
}
