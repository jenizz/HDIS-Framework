package tech.hdis.framework.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 成功返回消息配置
 *
 * @author 黄志文
 */
@Component
@ConfigurationProperties("hdis.response.error")
@Getter
@Setter
@ToString
public class ErrorProperties {
    /**
     * 默认错误返回消息，静态key
     */
    public static final String ERROR_KEY = "hdis.response.error.error";
    /**
     * 默认错误返回消息，静态value
     */
    public static String ERROR_VALUE;

    /**
     * 静态注入
     */
    @PostConstruct
    public void init() {
        ERROR_VALUE = this.getError();
    }

    /**
     * 默认成功返回消息
     */
    public String error = "interface execution error";
}
