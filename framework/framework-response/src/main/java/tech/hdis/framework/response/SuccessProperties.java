package tech.hdis.framework.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 成功返回消息配置
 *
 * @author 黄志文
 */
@Component
@ConfigurationProperties("hdis.response.success")
@Getter
@Setter
@ToString
public class SuccessProperties {
    /**
     * 默认成功返回消息，静态key
     */
    public static final String SUCCESS_KEY = "hdis.response.success.success";
    /**
     * 默认成功返回消息，静态value
     */
    public static String SUCCESS_VALUE;

    /**
     * 静态注入
     */
    @PostConstruct
    public void init() {
        SUCCESS_VALUE = this.getSuccess();
    }

    /**
     * 默认成功返回消息
     */
    public String success = "interface executed successfully";
}
