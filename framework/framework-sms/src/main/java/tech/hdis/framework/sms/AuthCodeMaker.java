package tech.hdis.framework.sms;

import java.util.Random;

/**
 * 验证码生成器
 *
 * @author 黄志文
 */
public class AuthCodeMaker {

    /**
     * 生成6位数验证码
     *
     * @return 6位验证码
     */
    public static String randNum6() {
        return String.valueOf(new Random().nextInt(899999) + 100000);
    }
}
