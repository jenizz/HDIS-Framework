package tech.hdis.framework.dictionary;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 数据字典，static工具类。
 *
 * @author 黄志文
 */
@Component
public class Dic {

    private static Environment environment;
    private static StringRedisTemplate stringRedisTemplate;

    @Autowired
    public void setEnvironment(Environment environment) {
        Dic.environment = environment;
    }

    @Autowired
    public void setEnvironment(StringRedisTemplate stringRedisTemplate) {
        Dic.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 获取数据字典
     *
     * @param key key
     * @return 值
     */
    public static String get(@NonNull String key) {
        String value = environment.getProperty(key);
        if (StringUtils.isEmpty(value)) {
            value = stringRedisTemplate.boundValueOps(key).get();
        }
        return value;
    }

    /**
     * 获取数据字典
     *
     * @param key key
     * @return 值
     */
    public static Long getLong(@NonNull String key) {
        return Long.valueOf(get(key));
    }

    /**
     * 获取数据字典
     *
     * @param key key
     * @return 值
     */
    public static Integer getInt(@NonNull String key) {
        return Integer.valueOf(get(key));
    }

    /**
     * 设置数据字典
     *
     * @param key   key
     * @param value value
     */
    public static void set(@NonNull String key, @NonNull String value) {
        stringRedisTemplate.boundValueOps(key).set(value);
    }
}