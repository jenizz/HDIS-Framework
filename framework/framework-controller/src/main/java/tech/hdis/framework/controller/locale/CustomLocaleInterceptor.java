package tech.hdis.framework.controller.locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import java.util.Locale;

/**
 * 自定义国际化配置，
 * 默认中文配置，可通过localeChangeInterceptor.setParamName()调节
 *
 * @author 黄志文
 */
@Configuration
public class CustomLocaleInterceptor extends WebMvcConfigurerAdapter {

    @Autowired
    private LocaleChangeInterceptor localeChangeInterceptor;

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleContextHolder.setLocale(Locale.CHINA);
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        return localeChangeInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        registry.addInterceptor(localeChangeInterceptor).addPathPatterns("/**");
    }
}
