package tech.hdis.framework.push.aliyun;

import com.aliyuncs.push.model.v20160801.PushRequest;
import com.aliyuncs.utils.ParameterHelper;
import lombok.NonNull;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 阿里云PushReques建造者增强
 *
 * @author 黄志文
 */
public class CustomPushRequest extends PushRequest {

    /**
     * 设备类型：Android
     */
    public static final String DEVICE_TYPE_ANDROID = "ANDROID";
    /**
     * 设备类型：IOS
     */
    public static final String DEVICE_TYPE_IOS = "iOS";
    /**
     * 设备类型：ALL
     */
    public static final String DEVICE_TYPE_ALL = "ALL";

    /**
     * 推送目标类型：ALL
     */
    public static final String TARGET_VALUE_TYPE_ALL = "ALL";

    /**
     * 推送目标：根据设备推送（最多支持1000个）
     */
    private static final String TARGET_DEVICE = "DEVICE";
    /**
     * 推送目标：根据账号推送（最多支持100个）
     */
    private static final String TARGET_ACCOUNT = "ACCOUNT";
    /**
     * 推送目标：根据别名推送（最多支持1000个）
     */
    private static final String TARGET_ALIAS = "ALIAS";
    /**
     * 推送目标：根据标签推送（最多支持1000个）
     */
    private static final String TARGET_TAG = "TAG";
    /**
     * 推送目标：推送给全部设备
     */
    private static final String TARGET_ALL = "ALL";

    /**
     * 推送类型：消息
     */
    private static final String MESSAGE_PUSH_TYPE = "MESSAGE";
    /**
     * 推送类型：通知
     */
    private static final String NOTICE_PUSH_TYPE = "NOTICE";

    /**
     * 建造
     */
    public static CustomPushRequest getInstance() {
        CustomPushRequest pushRequest = new CustomPushRequest();
        pushRequest.setAppKey(AliyunPushDic.getLone("hdis.aliyun.push.appKey"));
        pushRequest.setTarget(TARGET_ALIAS);
        pushRequest.setDeviceType(DEVICE_TYPE_ALL);
        pushRequest.setPushType(MESSAGE_PUSH_TYPE);
        pushRequest.setTitle(AliyunPushDic.get("hdis.aliyun.push.title"));
        pushRequest.setStoreOffline(true);
        String expireTime = ParameterHelper.getISO8601Time(new Date(System.currentTimeMillis() + 72 * 3600 * 1000));
        pushRequest.setExpireTime(expireTime);
        return pushRequest;
    }

    /**
     * 推送目标，默认别名推送
     *
     * @param target 推送目标
     * @return 推送对象
     */
    public CustomPushRequest target(@NonNull String target) {
        this.setTarget(target);
        return this;
    }

    /**
     * 推送类型
     *
     * @param pushType 推送类型
     * @return 推送对象
     */
    public CustomPushRequest pushType(@NonNull String pushType) {
        this.setPushType(pushType);
        return this;
    }

    /**
     * 推送内容
     *
     * @param body 推送内容
     * @return 推送对象
     */
    public CustomPushRequest body(@NonNull String body) {
        this.setBody(body);
        return this;
    }

    /**
     * 推送目标值
     *
     * @param targetValue 推送目标值
     * @return 推送对象
     */
    public CustomPushRequest targetValue(@NonNull String... targetValue) {
        this.setTargetValue(list2String(Arrays.asList(targetValue)));
        return this;
    }

    /**
     * list转逗号分隔的字符串工具
     *
     * @param list 原List
     * @return 带逗号字符串
     */
    private String list2String(@NonNull List<String> list) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            String num = list.get(i);
            builder.append(num);
            if (i != list.size() - 1) {
                builder.append(",");
            }
        }
        return builder.toString();
    }
}
