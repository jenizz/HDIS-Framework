<p align="center"><a align="center" href='https://gitee.com/w6513017/HDIS-Framework'><img src='https://gitee.com/w6513017/HDIS-Framework/widgets/widget_3.svg' alt='Fork me on Gitee'></img></a></p>
<h1 align="center">HDIS-Framework</h1>
<p align="center">HDIS-Framework是一个基于SpringBoot、Kubernetes、阿里云服务，编写的一个用于支撑微服务的极速开发框架。</p>
<p align="center">其文档详尽，Demo全面，设计合理，开箱即用，节省开发时间，提升开发效率。</p>
<p align="center">配套的docker、Kubernetes教程已踩过各种坑，让你的微服务无障碍的顺畅运行起来。</p>
<p align="center">HDIS与Kubernetes或SpringCloud配合使用，能达到最佳效果。</p>
<p align="center"><a href="https://gitee.com/w6513017/HDIS-Framework/wikis"><strong>《使用说明文档》</strong></a></p>
<p align="center"><a href="https://gitee.com/w6513017/HDIS-Demo"><strong>《Demo地址》</strong></a></p>

## 1.0项目介绍
用于支撑微服务的极速开发框架。<br>
同时也可以关注[《我的博客》](https://blog.csdn.net/qq_29994609)获取最新的技术分享。<br>
## 2.0软件架构
基于SpringBoot:1.5.13.RELEASE版本<br>
基于JDK1.8版本<br>
### 2.1框架整合pom
framework-starter-web：WebMvc的快速开发扩展框架<br>
framework-starter-data-jpa：JPA的快速开发扩展框架<br>
framework-starter-data-mongo：MongoDB的快速开发扩展框架<br>
framework-starter-cache：集群缓存、二级缓存、缓存工具的快速开发框架<br>
framework-starter-security：基于Oauth2.0的安全框架<br>
framework-starter-push：推送服务的快速开发框架<br>
framework-starter-sms：短信发送服务的快速开发框架<br>
framework-starter-test-benchmark：基准测试的快速开发框架<br>
### 2.2框架模块
framework-cache-redis：集群缓存、二级缓存、缓存工具的快速开发框架<br>
framework-controller：controller通用处理框架<br>
framework-data-jpa：JPA的快速开发扩展框架<br>
framework-data-mongo：MongoDB的快速开发扩展框架<br>
framework-dictionary：数据字典工具<br>
framework-exception：统一异常处理器<br>
framework-log：日志框架，日志输出到本地<br>
framework-log-aliyun：日志框架，日志输出到阿里云<br>
framework-page：统一分页工具<br>
framework-push：推送服务的统一接口定义。<br>
framework-push-aliyun:推送服务的阿里云实现。<br>
framework-response：统一返回值工具<br>
framework-security：基于Oauth2.0的安全框架<br>
framework-sms：短信服务的统一接口定义<br>
framework-sms-aliyun：短信服务的阿里云实现<br>
framework-swagger：swagger快速开发扩展框架<br>
framework-test：测试工具集，提供测试的常用工具<br>
framework-utils：常用工具集合<br>
## 3.0安装教程
### 3.1源码安装：使用maven编译安装到本地
```
git clone https://gitee.com/w6513017/HDIS-Framework.git
mvn clean install
```
### 3.2直接引用：使用maven引用
```
<!--parent引用-->
<parent>
    <groupId>tech.hdis</groupId>
    <artifactId>framework</artifactId>
    <version>1.3</version>
</parent>
<!--包引用-->
<dependencies>
    <dependency>
        <groupId>tech.hdis</groupId>
        <artifactId>framework-starter-web</artifactId>
    </dependency>
</dependencies>
<!--插件引用-->
<build>
    <plugins>
        <!--SpringBootMaven插件引用-->
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
        <!--解决配置文件中文乱码插件-->
        <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>native2ascii-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```
