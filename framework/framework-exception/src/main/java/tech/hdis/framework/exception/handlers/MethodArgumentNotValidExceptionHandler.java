package tech.hdis.framework.exception.handlers;


import org.springframework.web.bind.MethodArgumentNotValidException;
import tech.hdis.framework.exception.chain.AbstractExceptionHandlerChain;
import tech.hdis.framework.exception.exceptions.FieldError;
import tech.hdis.framework.exception.properties.ExceptionProperties;
import tech.hdis.framework.exception.response.ExceptionResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * hibernate验证统一异常返回器
 *
 * @author 黄志文
 */
public class MethodArgumentNotValidExceptionHandler extends AbstractExceptionHandlerChain {

    private static MethodArgumentNotValidExceptionHandler instance = new MethodArgumentNotValidExceptionHandler();

    private MethodArgumentNotValidExceptionHandler() {
    }

    public static MethodArgumentNotValidExceptionHandler getInstance() {
        return instance;
    }

    @Override
    public ExceptionResponse doHandler(Exception exception) {
        if (exception instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException validException = (MethodArgumentNotValidException) exception;
            List<FieldError> errors = new ArrayList<>();
            for (org.springframework.validation.FieldError error : validException.getBindingResult().getFieldErrors()) {
                FieldError myFieldError = new FieldError();
                myFieldError.setField(error.getField());
                myFieldError.setMessage(error.getDefaultMessage());
                errors.add(myFieldError);
            }
            return ExceptionResponse.getInstance().code(ExceptionProperties.VALIDATOR_KEY, ExceptionProperties.VALIDATOR_VALUE).fieldErrors(errors);
        }
        return this.getNextHandler().doHandler(exception);
    }
}
