package tech.hdis.framework.exception.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 异常返回消息属性
 *
 * @author 黄志文
 */
@Component
@ConfigurationProperties("hdis.response.exception")
@Getter
@Setter
@ToString
public class ExceptionProperties {
    /**
     * 业务异常返回消息，静态key
     */
    public static final String BUSINESS_KEY = "hdis.response.exception.business";
    /**
     * 默认异常返回消息，静态key
     */
    public static final String DEFAULTED_KEY = "hdis.response.exception.defaulted";
    /**
     * 默认验证异常返回消息，静态key
     */
    public static final String VALIDATOR_KEY = "hdis.response.exception.validator";
    /**
     * 默认异常返回消息，静态value
     */
    public static String DEFAULTED_VALUE;
    /**
     * 默认验证异常返回消息，静态value
     */
    public static String VALIDATOR_VALUE;

    /**
     * 静态注入
     */
    @PostConstruct
    public void init() {
        DEFAULTED_VALUE = getDefaulted();
        VALIDATOR_VALUE = getValidator();
    }

    /**
     * 默认异常返回消息
     */
    public String defaulted = "the system is error, please try again later.";
    /**
     * 默认验证异常返回消息
     */
    public String validator = "input error, please check the input field.";
}
