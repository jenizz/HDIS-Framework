package tech.hdis.framework.exception.handlers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.validation.BindException;
import tech.hdis.framework.exception.chain.AbstractExceptionHandlerChain;
import tech.hdis.framework.exception.exceptions.BusinessException;
import tech.hdis.framework.exception.exceptions.SystemException;
import tech.hdis.framework.exception.properties.ExceptionProperties;
import tech.hdis.framework.exception.response.ExceptionResponse;

/**
 * 校验异常处理器
 *
 * @author 黄志文
 */
public class SystemExceptionHandler extends AbstractExceptionHandlerChain {

    private static SystemExceptionHandler instance = new SystemExceptionHandler();

    private SystemExceptionHandler() {
    }

    public static SystemExceptionHandler getInstance() {
        return instance;
    }

    private static final Logger logger = LoggerFactory.getLogger(SystemExceptionHandler.class);
    private static final Marker SYSTEM_EXCEPTION_MAKER = MarkerFactory.getMarker("SystemException");

    @Override
    public ExceptionResponse doHandler(Exception exception) {
        //不收集的异常
        boolean exclude = exception instanceof BusinessException || exception instanceof BindException;
        if (!exclude) {
            logger.error(SYSTEM_EXCEPTION_MAKER, ExceptionProperties.DEFAULTED_VALUE, new SystemException(ExceptionProperties.DEFAULTED_KEY, ExceptionProperties.DEFAULTED_VALUE, exception));
        }
        return ExceptionResponse.getInstance().code(ExceptionProperties.DEFAULTED_KEY, ExceptionProperties.DEFAULTED_VALUE);
    }
}
