package tech.hdis.framework.exception.chain;


import tech.hdis.framework.exception.response.ExceptionResponse;

/**
 * 末尾占位过滤器
 *
 * @author 黄志文
 */
public class LastExceptionHandler extends AbstractExceptionHandlerChain {

    private static LastExceptionHandler instance = new LastExceptionHandler();

    private LastExceptionHandler() {
    }

    public static LastExceptionHandler getInstance() {
        return instance;
    }

    @Override
    public ExceptionResponse doHandler(Exception exception) {
        return null;
    }
}
