package tech.hdis.framework.oss.aliyun;

import com.aliyun.oss.OSSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云OSS配置
 *
 * @author 黄志文
 */
@Configuration
public class AliyunOSSConfig {

    @Autowired
    private AliyunOSSProperties aliyunOSSProperties;

    /**
     * 阿里云OSS客户端生成
     *
     * @return 阿里云OSS客户端
     */
    @Bean
    public OSSClient ossClient() {
        OSSClient ossClient = new OSSClient(aliyunOSSProperties.getEndpoint(), aliyunOSSProperties.getAccessKeyId(), aliyunOSSProperties.getAccessKeySecret());
        return ossClient;
    }
}
