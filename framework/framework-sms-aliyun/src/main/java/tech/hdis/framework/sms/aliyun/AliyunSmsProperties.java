package tech.hdis.framework.sms.aliyun;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * 阿里云推送参数配置
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
@Component
@Validated
@ConfigurationProperties("hdis.aliyun.sms")
public class AliyunSmsProperties {
    /**
     * 阿里云accessKeyId
     */
    @NotBlank(message = "'hdis.aliyun.sms.accessKey' property can not be null.please find it in your aliyun.")
    private String accessKeyId;
    /**
     * 阿里云accessKeySecret
     */
    @NotBlank(message = "'hdis.aliyun.sms.accessSecret' property can not be null.please find it in your aliyun.")
    private String accessKeySecret;
    /**
     * 阿里云短信签名
     */
    @NotBlank(message = "'hdis.aliyun.sms.signName' property can not be null.please find it in your aliyun.")
    private String signName;
    /**
     * 阿里云验证码短信模板号
     */
    private String authTemplateCode;
}
