package tech.hdis.framework.sms.aliyun;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * 阿里云验证码实现
 *
 * @author 黄志文
 */
@Configuration
public class AliyunSMSConfig {

    private static final String REGION_ID = "cn-hangzhou";
    private static final String ENDPOINT = "cn-hangzhou";
    private static final String PRODUCT = "Dysmsapi";
    private static final String DOMAIN = "dysmsapi.aliyuncs.com";

    @Resource
    private AliyunSmsProperties aliyunSmsProperties;

    @Bean
    public IAcsClient iAcsClient() throws Exception {
        //组装发送客户端
        IClientProfile profile = DefaultProfile.getProfile(REGION_ID, aliyunSmsProperties.getAccessKeyId(), aliyunSmsProperties.getAccessKeySecret());
        DefaultProfile.addEndpoint(ENDPOINT, REGION_ID, PRODUCT, DOMAIN);
        return new DefaultAcsClient(profile);
    }
}
