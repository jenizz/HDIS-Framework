package tech.hdis.framework.security.annotation;

import java.lang.annotation.*;

/**
 * 登录标记
 * 被此方法标记了的方法需要已登录
 *
 * @author 黄志文
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SessionMark {

    //角色字符串
    String[] roles() default "";

    //权限字符串
    String[] permissions() default "";

    //是否开启单点登录
    boolean isSingle() default false;
}