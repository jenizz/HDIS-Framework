package com.nokia.sdn.spn.util;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

/**
 * 性能测试时间记录工具
 *
 * @author 黄志文
 */
@Getter
@Setter
public class Timer {

    private static final ThreadLocal<Timer> TIMER_THREAD_LOCAL = new ThreadLocal<>();

    private Long startTime;

    public static void start() {
        Timer timer = new Timer();
        timer.startTime = System.currentTimeMillis();
        TIMER_THREAD_LOCAL.set(timer);
    }

    public static Long costTimeMillis() {
        Long currentTimeMillis = System.currentTimeMillis();
        Long startTime = Optional.ofNullable(TIMER_THREAD_LOCAL.get()).map(Timer::getStartTime).orElse(currentTimeMillis);
        return currentTimeMillis - startTime;
    }

    public static Long costTimeSeconds() {
        return costTimeMillis() / 1000;
    }
}
