package tech.hdis.framework.utils.security;

import lombok.NonNull;

/**
 * 16进制字符串转化工具
 *
 * @author 黄志文
 */
public class HexStringUtils {

    /**
     * 全局数组
     */
    private final static String[] HEX_DIGITS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    /**
     * 将一个字节转化成十六进制形式的字符串
     *
     * @param b 字节数组
     * @return 字符串
     * @author 黄志文
     */
    public static String byteToHexString(@NonNull byte b) {
        int ret = b;
        if (ret < 0) {
            ret += 256;
        }
        int m = ret / 16;
        int n = ret % 16;
        return HEX_DIGITS[m] + HEX_DIGITS[n];
    }

    /**
     * 转换字节数组为十六进制字符串
     *
     * @param bytes 字节数组
     * @return 十六进制字符串
     * @author 黄志文
     */
    public static String byteArrayToHexString(@NonNull byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(byteToHexString(bytes[i]));
        }
        return sb.toString();
    }
}
