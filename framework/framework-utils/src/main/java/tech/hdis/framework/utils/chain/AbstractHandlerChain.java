package tech.hdis.framework.utils.chain;


import lombok.NonNull;

/**
 * 抽象责任链模式-链表
 *
 * @author 黄志文
 */
public abstract class AbstractHandlerChain<T, P> implements Handler<T, P> {

    private Handler nextHandler;

    @Override
    public Handler getNextHandler() {
        return this.nextHandler;
    }

    @Override
    public void setNextHandler(@NonNull Handler nextHandler) {
        this.nextHandler = nextHandler;
    }
}
