package tech.hdis.framework.utils.tool;

/**
 * UUID工具类
 *
 * @author 黄志文
 */
public class UUID {

    /**
     * UUID生成器。
     * 去掉java生成的UUID中的"-"。
     *
     * @return UUID
     */
    public static String getId() {
        java.util.UUID uuid = java.util.UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }

}

