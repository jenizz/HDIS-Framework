package ${basePackage}.repository;

import ${basePackage}.core.po.${entityName};
import org.springframework.stereotype.Repository;
import tech.hdis.framework.data.jpa.repository.JpaPoRepository;

/**
 * ${entityNameAnnotation}
 *
 * @author 黄志文
 */
@Repository
public interface ${entityName}Repository extends MongoPoRepository<${entityName}> {
}

