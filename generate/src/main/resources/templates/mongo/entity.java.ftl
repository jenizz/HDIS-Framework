package ${basePackage}.core.po;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Field;
import tech.hdis.framework.data.mongo.po.MongoDataEntity;

/**
 * ${entityNameAnnotation}
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
@Document(collection = "")
public class ${entityName} extends MongoDataEntity {
    @Field("name")
    private String name;
}
