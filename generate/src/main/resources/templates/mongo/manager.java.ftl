package ${basePackage}.manager;

import ${basePackage}.core.po.${entityName};
import ${basePackage}.repository.mongo.${entityName}Repository;
import org.springframework.stereotype.Component;
import tech.hdis.framework.data.mongo.manager.MongoDataManager;

/**
 * ${entityNameAnnotation}
 *
 * @author 黄志文
 */
@Component
public class ${entityName}Manager extends MongoDataManager<${entityName}Repository, ${entityName}> {
}