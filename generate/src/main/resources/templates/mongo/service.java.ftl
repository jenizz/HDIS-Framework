package ${basePackage}.service;

import ${basePackage}.manager.${entityName}Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ${entityNameAnnotation}
 *
 * @author 黄志文
 */
@Service
public class ${entityName}Service {

    @Autowired
    private ${entityName}Manager manager;
}