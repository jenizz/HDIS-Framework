package ${basePackage}.test.controller;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import ${basePackage}.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tech.hdis.framework.dictionary.Dic;
import tech.hdis.framework.response.RestfulResponse;
import tech.hdis.framework.restful.Restful;
import tech.hdis.framework.test.JsonPath;
import tech.hdis.framework.test.MockRestfulRequestFactory;
import tech.hdis.framework.test.ResponseValue;
import tech.hdis.framework.utils.json.JSONUtils;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ${entityName}ControllerTest {

    private static final String PARENT_PATH = "[controller的URL]";

    @Autowired
    private MockMvc mockMvc;

    private String accessToken;

    /**
     * 登录
     *
     * @throws Exception
     */
    @Before
    public void loginSuccess() throws Exception {
        String url = "[url]";
        ResponseEntity<String> responseEntity = Restful.postForObject(url, param, String.class);
        RestfulResponse restfulResponse = JSONUtils.fromJson(responseEntity.getBody(), RestfulResponse.class);
        accessToken = (String) restfulResponse.getData().get("token");
    }

    /**
     *
     *
     * @throws Exception
     */
    @Test
    public void postTest() throws Exception {
        String url = "[url]";
        mockMvc.perform(MockRestfulRequestFactory.post(PARENT_PATH + url, param).header("access_token", accessToken))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.CODE).value(ResponseValue.SUCCESS_CODE))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.MESSAGE).value(ResponseValue.SUCCESS_VALUE))
        ;
    }

    /**
     *
     *
     * @throws Exception
     */
    @Test
    public void getTest_Success() throws Exception {
        String url = "[url]";
        mockMvc.perform(MockRestfulRequestFactory.get(PARENT_PATH + url)
                .param("param", param)
                .header("access_token", accessToken))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.CODE).value(ResponseValue.SUCCESS_CODE))
                .andExpect(MockMvcResultMatchers.jsonPath(JsonPath.MESSAGE).value(ResponseValue.SUCCESS_VALUE))
        ;
    }

}
