package ${basePackage}.controller;

import ${basePackage}.service.${entityName}Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tech.hdis.framework.response.RestfulResponse;

/**
 * ${entityNameAnnotation}
 *
 * @author 黄志文
 */
@Api(tags = "${entityNameAnnotation}")
@RestController
@RequestMapping("/api/v1/[Controller名称]")
public class ${entityName}Controller {

    @Autowired
    private ${entityName}Service service;

    @ApiOperation(value = "")
    @PostMapping("")
    public RestfulResponse save(@RequestBody @Validated DTO DTO) {
        return RestfulResponse.getInstance().success();
    }

    @ApiOperation(value = "")
    @PutMapping("")
    public RestfulResponse update() {
        return RestfulResponse.getInstance().success();
    }

    @ApiOperation(value = "")
    @DeleteMapping("")
    public RestfulResponse delete() {
        return RestfulResponse.getInstance().success();
    }

    @ApiOperation(value = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "", value = "", required = true),
    })
    @GetMapping("")
    public RestfulResponse find() {
        return RestfulResponse.getInstance().success();
    }
}