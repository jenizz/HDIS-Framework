package tech.hdis.generate;

import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * 启动类
 *
 * @author 黄志文
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private Configuration configuration;

    /**
     * 文件输出位置
     */
    private static final String FILE_PACKAGE = "C:\\1-project\\generate\\";
    /**
     * 代码中基础包
     */
    private static final String BASE_PACKAGE = "XXX";
    /**
     * 实体名称
     */
    private static final String ENTITY_NAME = "XXX";
    /**
     * 实体中文注释
     */
    private static final String ENTITY_NAME_ANNOTATION = "XXX";
    /**
     * 数据库类型
     */
    private static final String DATABASE_TYPE = "mongo";

    /**
     * 代码生成参数
     *
     * @return 参数MAP
     */
    private Map<String, Object> getParams() {
        Map<String, Object> params = new HashMap<>(16);
        params.put("basePackage", BASE_PACKAGE);
        params.put("entityName", ENTITY_NAME);
        params.put("entityNameAnnotation", ENTITY_NAME_ANNOTATION);
        return params;
    }

    /**
     * 生成MongoEntity代码
     */
    @SneakyThrows
    private void generateMongoEntity() {
        Template repositoryTemplate = configuration.getTemplate("jpa\\entity.java.ftl");
        @Cleanup Writer repositoryFile = new FileWriter(new File(FILE_PACKAGE + ENTITY_NAME + ".java"));
        repositoryTemplate.process(getParams(), repositoryFile);
    }

    /**
     * 生成JpaRepository代码
     */
    @SneakyThrows
    private void generateJpaRepository() {
        Template repositoryTemplate = configuration.getTemplate("jpa\\repository.java.ftl");
        @Cleanup Writer repositoryFile = new FileWriter(new File(FILE_PACKAGE + ENTITY_NAME + "Repository.java"));
        repositoryTemplate.process(getParams(), repositoryFile);
    }

    /**
     * 生成MongoRepository代码
     */
    @SneakyThrows
    private void generateMongoRepository() {
        Template repositoryTemplate = configuration.getTemplate("mongo\\repository.java.ftl");
        @Cleanup Writer repositoryFile = new FileWriter(new File(FILE_PACKAGE + ENTITY_NAME + "Repository.java"));
        repositoryTemplate.process(getParams(), repositoryFile);
    }

    /**
     * 生成MongoManager代码
     */
    @SneakyThrows
    private void generateMongoManager() {
        Template repositoryTemplate = configuration.getTemplate("mongo\\manager.java.ftl");
        @Cleanup Writer repositoryFile = new FileWriter(new File(FILE_PACKAGE + ENTITY_NAME + "Manager.java"));
        repositoryTemplate.process(getParams(), repositoryFile);
    }

    /**
     * 生成JpaService代码
     */
    @SneakyThrows
    private void generateJpaService() {
        Template serviceTemplate = configuration.getTemplate("jpa\\service.java.ftl");
        @Cleanup Writer serviceFile = new FileWriter(new File(FILE_PACKAGE + ENTITY_NAME + "Service.java"));
        serviceTemplate.process(getParams(), serviceFile);
    }

    /**
     * 生成MongoService代码
     */
    @SneakyThrows
    private void generateMongoService() {
        Template serviceTemplate = configuration.getTemplate("mongo\\service.java.ftl");
        @Cleanup Writer serviceFile = new FileWriter(new File(FILE_PACKAGE + ENTITY_NAME + "Service.java"));
        serviceTemplate.process(getParams(), serviceFile);
    }

    /**
     * 生成Controller代码
     */
    @SneakyThrows
    private void generateController() {
        Template controllerTemplate = configuration.getTemplate("controller.java.ftl");
        @Cleanup Writer controllerFile = new FileWriter(new File(FILE_PACKAGE + ENTITY_NAME + "Controller.java"));
        controllerTemplate.process(getParams(), controllerFile);
    }

    /**
     * 生成Test代码
     */
    @SneakyThrows
    private void generateTest() {
        Template controllerTemplate = configuration.getTemplate("test.java.ftl");
        @Cleanup Writer controllerFile = new FileWriter(new File(FILE_PACKAGE + ENTITY_NAME + "ControllerTest.java"));
        controllerTemplate.process(getParams(), controllerFile);
    }

    /**
     * 初始化执行器
     */
    @PostConstruct
    private void generate() {
        if ("mysql".equals(DATABASE_TYPE)) {
            generateJpaRepository();
            generateJpaService();
        }
        if ("mongo".equals(DATABASE_TYPE)) {
            generateMongoRepository();
            generateMongoManager();
            generateMongoService();
        }
        generateController();
        generateTest();
    }
}
